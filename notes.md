Start a new project: 

 - make sure you have *node* installed and run:
  `npx create-react-app my-app`
`cd my-app`
`npm install`
 - install a package and check *package.json*

Create the first component

 - [components](https://react.dev/learn#components)
 - JSX syntax
 - styles
 - conditional rendering
 - rendering lists
 - event handlers
 - state, props
