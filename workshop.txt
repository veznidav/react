look at: 
1. Task in typescript
2. stabilize functions using useCallback
3. react-workshop branch vse z workshopum, vsechny snipety
4. useLayouEffect
5. memoization in JS
6. reacttraining.com/tree
7. useContext more performant (komponenta pod nim by mela byt memoized) (screenshot)
8. a curse of react, reach ui 
9. compound components , zamerit se na tu variantu, kde je pouziti context
10. forwardRef
11. component.displayName - je to kvuli nazvu v react dev tools
12. useOpaque... // pro a11y
13. render props
14. podivat se na wrapEvent, obecne to muze byt dobre pro vice handleru na jeden onclick



notes
- mame v effectu fetch, uzivatel rychle zavre subpage s kompoentou - muze se stat, ze bude state update na unmount komponente - to vyhodi chybu 
	- ve screenu je reseni s cleanup funkci
	- pomuze to i proti race conditions kdyz uzivatel treba rychle preklikava nejaky nav ktery na kazdy klik udela request
	- subscribe/unsubscribe - to je proc se to musi delat - aby se neupdatoval stav na unmmount komponente
- useEffect [param] - tohle rika, jet znova effect, kdyz predchozi param je jiny, nez soucasny param	
- co se dava to useEffect pole
	- any variable that we close over that can change
	- to avoid stale values in closers - kdyz se funkce s promennema z jedne scope vezme a spusti v jine scope - ma kopii puvodnich hodnot
	- takze i funkce, ktere se mohou menit, musi byt v poli (treba kdyz jde funkce z props). linter nerozpozna, jestli se fce z props muze menit, proto to vzdy chce funkce z props aby byly v poli
	- useState enni potrba mit v poli, protoze react garantuje, ze je to porad stejna fce i mezi rerenders
- kdyz predavame prop funkci jako () => api.fetch()... tak vzdy je to nova funkce. 
	- to je proc potom pouzivame useCallback - vezme funkci a nevola ji, on ji pouze vraci v stable way. tzn ze se nedela nova fce v pameti, ale pouziva se ta stejna. diky tomu ti potom v child komonente neprobehne tolikrat effect, ktery ma v poli tuto funkci
- rerender neznamena, ze se DOM vzdy zmeni. je to jen moznost pro komponentu neco zmenit. kdyz se nic nemeni, tak dom se take nemeni		
- toto vsechno jsou side effects: window, document, network, localStorage, cookies, working with dom directly . melo by to jit od useEffect. Pomuckou je rict si, jestli ta dana cast pobezi na serveru. kdyz ne, tak useEffect
- kdyz neco skryjeme pomoci CSS, tak komponenty porad jsou namountovane a muzou delat side efekty
- useMemo zavola passnutou funkci (narozdil od useCallback). useMemo stabilizuje hodnotu, useCallback stabilizuje funkci
- useLayouEffect - bezi take na konci render faze, layouEffect bezi jeste pred vykreslenim. "pocka si na to, co udelas se stavem a pak az to vykresli." da se tak predejit nejakemu probliknuti apod. ale nefunguje to pro fetchovani dat, tam to neceka.
- lepsi jsou 'refs' nez getelementbyid, nemusi se delat idecka
- memoizing
	- mas pomalou fci, ktera bezi na kazdy render. useMemo to zrychli
	- kdyz je input stejny, tak se fce znovu neprobehne ale vrati zapamatovany vysledek
	- react.memo
		- parent neco zmeni, vsechny children dostanou rerender
		- class based - purecomponent se podiva na to, jestli se props zmenili a kdyz ne, tak neudela rerender
		- function component - pouzijeme React.memo. - kdyz jsou props jine, tak rendereuje, jinak ne 
	- useCallback je memoaization pro fce, useMemo pro hodnoty
- context
	- mame providera, kterej informuje vsechny svoje konzumenty o zmene
	- diky tomu nedochazi ke kaskadovym rerenderum, protoze rerenderuji pouze konzumenti
	- kdyz mam vice kontextu, tak se podivat do index.tsx v ProjectPlanner
- advanced composition
	- curse of react - vse je mene accessible, protoze si vytvarime vlastni custom komponenty a nepouzivame tolik pripravene HTML tagy
	- Compound components
		- diky tomu muzeme napriklad menit poradi subkomponent
		- kdyz delam sdilenou komponentu, tak ji udelat higly composable protoze nevime, co vsechno do ni v budoucnu bude potreba pridat